FROM maven:3.5.0-jdk-8

WORKDIR /app


#COPY package*.json ./


#RUN javac DemoApplicationTests.java
#RUN java DemoApplicationTests.java

COPY . .

EXPOSE 8080
CMD [ "DemoApplicationTests.java" ]
